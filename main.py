from random import randint

number = randint(1,100)

essai = 1

while essai <= 10:
    print(f"Essai {essai}")
    guess = int(input("Entrez votre nombre: "))
    if guess == number:
        print(f"Bravo! Vous avez trouvé après {essai} essai(s).\n")
        break
    else:
        if essai < 10:
            print("Faux!")
            if number < guess:
                print("Mon nombre est plus petit.\n")
            else:
                print("Mon nombre est plus grand.")
        else:
            print("Perdu!")
        essai += 1